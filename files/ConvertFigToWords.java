import java.util.HashMap;
import java.util.Scanner;

public class ConvertFigToWords {
private HashMap<Integer,String[]> hashMap;

public ConvertFigToWords()
{
	hashMap=new HashMap<Integer,String[]>();
	fillTheMap();
}
private void fillTheMap()
{
  ones();
  tens();
  hundreds();
  thousands();
  tenThousands();
  lakh();
  
  //tenLakh();
  //crore();
}
private void ones()
{
	String[] ones=new String[] {"Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine"};
    hashMap.put(1,ones);	

}
private void tens()
{
	String[] tens=new String[] {"Ten","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"};
    hashMap.put(2,tens);	
}
private void hundreds()
{
	String[] hundreds=new String[] {"OneHundred","TwoHundred","ThreeHundred","FourHundred","FiveHundred","SixHundred","SevenHundred","EightHundred","NineHundred"};
    hashMap.put(3,hundreds);	
}
private void thousands()
{
	String[] thousands=new String[] {"OneThousand","TwoThousand","ThreeThousand","FourThousand","FiveThousand","SixThousand","SevenThousand","EightThousand","NineThousand"};
    hashMap.put(4,thousands);	
}
private void tenThousands()
{
	String[] tenThousands=new String[] {"TenThousand","TwentyThousand","ThirtyThousand","FortyThousand","FiftyThousand","SixtyThousand","SeventyThousand","EightyThousand","NinetyThousand"};
    hashMap.put(5,tenThousands);
}
private void lakh()
{
	String[] lakh=new String[] {"OneLakh","TwoLakh","ThreeLakh","FourLakh","FiveLakh","SixLakh","SevenLakh","EightLakh","NineLakh"};
    hashMap.put(6, lakh);
}

public static void main(String[]args)
{
	Scanner sc=new Scanner(System.in);
	int number=sc.nextInt();
	int total=0;
	ConvertFigToWords convert=new ConvertFigToWords();
	String result="";
	while(number!=0)
	{
		int digit=number%10;
		total++;
		if(total!=1)
		result=convert.hashMap.get(total)[digit-1]+result;
		else
            result=convert.hashMap.get(total)[digit]+result;
		number=number/10;
	}
	System.out.println(result);
}
}
