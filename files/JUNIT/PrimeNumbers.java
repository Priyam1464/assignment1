import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PrimeNumbers
{

 @Test
 public void numLessThanTwo()
 {
  CheckNum number=new CheckNum(-5);
  Assertions.assertEquals(false,number.check());
 }
 @Test
 public void numIsTwo()
 {
  CheckNum number=new CheckNum(2);
  Assertions.assertEquals(true,number.check());
 }
@Test
 public void numisEven()
{
 CheckNum number=new CheckNum(10);
 Assertions.assertEquals(false,number.check());
}
@Test
public void numisOdd()
{
 CheckNum number=new CheckNum(13);
 Assertions.assertEquals(true,number.check());
}
}
