package MVC;

public class AddTwoNumbersModel {
    private int num1,num2,sum;

    public int getNum1() {
        return num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
    public void finalize()//can call explicitly
    {
        num1=0;
        num2=0;
        sum=0;
        System.out.println("I am in destructor");
    }
}
