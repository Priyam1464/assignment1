package MVC;

public class BusinessLogic {
    public void  compute(AddTwoNumbersModel addTwoNumbersModel)
    {
        addTwoNumbersModel.setSum(addTwoNumbersModel.getNum1()+addTwoNumbersModel.getNum2());
    }
}
