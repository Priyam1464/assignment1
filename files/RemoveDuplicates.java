import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class RemoveDuplicates {
    public HashSet<Integer> hashSet;
    public int[]arr;
    public HashMap<Integer,Integer>hashMap;
    public RemoveDuplicates(int[] arr)
    {
        this.arr=arr;
        this.hashSet=new HashSet<>();
        this.hashMap=new HashMap<>();
    }
    public void pushToSet(int[] arr)
    {
        for(int num:arr)
        {
            hashSet.add(num);
        }
    }

    public void  pushToMap()
    {
        for(int num:arr)
        {
            hashMap.put(num,hashMap.get(num)+1);
        }
    }
    public void display()
    {
        Iterator<Integer> iterator=hashSet.iterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
    public void display2()
    {
        Iterator<Map.Entry<Integer,Integer>> itr = hashMap.entrySet().iterator();
        while(itr.hasNext())
        {
            Map.Entry<Integer, Integer> entry = itr.next();
            if(entry.getValue()<=1) System.out.println(entry.getKey());
        }
    }
    public static void main(String[] args)
    {
        int[] arr=new int[]{2,4,5,6,1,1,2,4,5,6,7,8,9};
        RemoveDuplicates removeDuplicates=new RemoveDuplicates(arr);
        removeDuplicates.pushToSet(arr);
        removeDuplicates.pushToMap();
        removeDuplicates.display();
        System.out.println();
        removeDuplicates.display2();

    }
}
